package com.freakypunker;

public class Main {

    public static void main(String[] args) {
	    int count = 0;
	    while(count != 5){
            System.out.println("count is " + count);
            count++;
        }

        count = 1;
	    do {
            System.out.println("count is " + count);
            count++;
        } while (count != 6);

	    int checkNumber = 5;
	    int lastNumber = 20;
	    int counter = 0;

        while(checkNumber <= lastNumber) {
            if(!isEvenNumber(checkNumber)) {
                checkNumber++;
                continue;
            }
            counter++;
            System.out.println("Even number " + checkNumber);
            checkNumber++;
            if(counter >= 5){
                break;
            }
        }
    }

    public static boolean isEvenNumber(int number) {
        if((number % 2) == 0) {
            return true;
        } else {
            return false;
        }
    }
}
