package com.freakypunker;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Account carmineAccount = new Account("1",100.0,"Carmine","carmine@email.com","12345678900");

        carmineAccount.withdrawal(50.0);
        carmineAccount.deposit(100.0);

        double remainingBalance = carmineAccount.getBalance();
        System.out.println(remainingBalance);

        VipCustomer newVipCustomer = new VipCustomer("Carmine", 1000.0, "carmine@email.com");

        System.out.println("vip customer name " + newVipCustomer.getName() + "credit limit " + newVipCustomer.getCreditLimit() + " email address " + newVipCustomer.getEmail());
    }
}
