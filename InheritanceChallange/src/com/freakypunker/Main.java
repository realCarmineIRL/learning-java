package com.freakypunker;

public class Main {

    public static void main(String[] args) {
        Outlander newOutlander = new Outlander(36);
        newOutlander.steer(45);
        newOutlander.accelerate(30);
        newOutlander.accelerate(20);
        newOutlander.accelerate(-42);
    }
}
