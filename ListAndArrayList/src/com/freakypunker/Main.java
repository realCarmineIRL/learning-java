package com.freakypunker;

import javax.sound.midi.Soundbank;
import java.util.Scanner;

public class Main {
    private static Scanner scanner = new Scanner(System.in);
    private static MobilePhone mobilePhone = new MobilePhone("0831533083");

    public static void main(String[] args) {
        boolean quit = false;
        startPhone();
        printActions();
        while(!quit) {
            System.out.println("\nEnter action: (6 show menu)");
            int action = scanner.nextInt();
            scanner.nextLine();

            switch (action) {
                case 0:
                    shutdown();
                    quit = true;
                    break;
                case 1:
                    mobilePhone.printContacts();
                    break;
                case 2:
                    addNewContact();
                    break;
                case 3:
                    updateContact();
                    break;
                case 4:
                    removeContact();
                    break;
                case 5:
                    queryContact();
                    break;
                case 6:
                    printActions();
                    break;
            }
        }

    }

    private static void startPhone() {
        System.out.println("Starting phone...");
    }

    private static void shutdown() {
        System.out.println("phone is shutting down...");
    }

    private static void addNewContact() {
        System.out.println("Enter new contact name: ");
        String name = scanner.nextLine();
        System.out.println("Enter new contact phone: ");
        String phoneNumber = scanner.nextLine();

        Contact newContact = Contact.createContact(name,phoneNumber);

        if(mobilePhone.addNewContact(newContact)) {
            System.out.println("New contact added: name = " + name + ", phone = " + phoneNumber);
        }else {
            System.out.println("Cannot add " + name + " already in contacts");
        }
    }

    private static void updateContact() {
        System.out.println("Enter contact name: ");
        String name = scanner.nextLine();

        Contact existingContact = mobilePhone.queryContact(name);
        if(existingContact == null) {
            System.out.println("Contact not found");
            return;
        }

        System.out.println("Enter new contact name: ");
        name = scanner.nextLine();
        System.out.println("Enter new contact phone: ");
        String phoneNumber = scanner.nextLine();

        Contact newContact = Contact.createContact(name, phoneNumber);

        if(mobilePhone.updateContact(existingContact, newContact)) {
            System.out.println("Contact has been updated");
        }else {
            System.out.println("Error, contact was not updated");
        }
    }

    private static void removeContact() {
        System.out.println("Enter contact name: ");
        String name = scanner.nextLine();

        Contact existingContact = mobilePhone.queryContact(name);
        if(existingContact == null) {
            System.out.println("Contact not found");
            return;
        }

        if(mobilePhone.removeContact(existingContact)) {
            System.out.println("Contact removed");
        }else {
            System.out.println("Error, contact was not removed");
        }
    }

    private static void queryContact() {
        System.out.println("Enter contact name: ");
        String name = scanner.nextLine();

        Contact existingContact = mobilePhone.queryContact(name);

        if(existingContact == null) {
            System.out.println("Contact does not exists");
            return;
        }

        System.out.println("Name = " + existingContact.getName() + ", phone = " + existingContact.getPhoneNumber());
    }

    private static void printActions(){
        System.out.println("\nAvailable actions:\npress");
        System.out.println( "0 - shutdown\n" +
                            "1 - show contacts\n" +
                            "2 - add new contact\n" +
                            "3 - update existing contact\n" +
                            "4 - remove contact\n" +
                            "5 - check if contact exists\n" +
                            "6 - show menu");
        System.out.println("Choose your option: ");
    }
}
