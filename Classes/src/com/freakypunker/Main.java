package com.freakypunker;

public class Main {

    public static void main(String[] args) {
	    Car porsche = new Car();

	    porsche.setModel("Carrera");

	    String carModel = porsche.getModel();
        System.out.println("the car model is " + carModel);
    }
}