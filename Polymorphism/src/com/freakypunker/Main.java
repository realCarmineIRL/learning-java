package com.freakypunker;

public class Main {

    public static void main(String[] args) {
	    Car car = new Car(8,"base car");

        System.out.println(car.startEngine());
        System.out.println(car.accelerate());
        System.out.println(car.brake());

        Mitsubishi mitsubishi = new Mitsubishi(4,"eclipse");

        System.out.println(mitsubishi.startEngine());
        System.out.println(mitsubishi.accelerate());
        System.out.println(mitsubishi.brake());

        LandRover landRover = new LandRover(8,"Discovery");

        System.out.println(landRover.startEngine());
        System.out.println(landRover.accelerate());
        System.out.println(landRover.brake());
    }
}
