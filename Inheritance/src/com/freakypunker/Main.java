package com.freakypunker;

public class Main {

    public static void main(String[] args) {
	    Animal animal = new Animal(1,1,5,5, "Animal");

	    Dog newDog = new Dog(8, 20, "Yorkie", 2, 4, 1, 20, "long silky");

	    newDog.eat();

	    newDog.walk();
	    newDog.run();

	    Fish newFish = new Fish(3,5,"Shark", 2,2,1);

	    newFish.swim(15);
    }
}
