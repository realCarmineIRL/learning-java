package com.freakypunker;

public class Main {

    public static void main(String[] args) {

        int newScore = calculateScore("Tim", 500);
        System.out.println("New score " + newScore);
        calculateScore(75);
        calculateScore();

        calculateFeetAndInchesToCentimeters(6,0);
        calculateFeetAndInchesToCentimeters(7,5);
        calculateFeetAndInchesToCentimeters(6,-10);
        calculateFeetAndInchesToCentimeters(6,-13);

        calculateFeetAndInchesToCentimeters(100);
        calculateFeetAndInchesToCentimeters(157);
        calculateFeetAndInchesToCentimeters(-10);
    }

    public static int calculateScore(String playerName, int score) {
        System.out.println("Player " + playerName + " score " + score + " pts");
        return score * 1000;
    }

    public static int calculateScore(int score) {
        System.out.println("Unknown Player score " + score + " pts");
        return score * 1000;
    }

    public static int calculateScore() {
        System.out.println("Unknown Player no score pts");
        return 0;
    }

    public static double calculateFeetAndInchesToCentimeters(double feet, double inches){
        if((feet < 0) || ((inches < 0) || (inches > 12))) {
            System.out.println("invalid feet or inches parameters");
            return -1;
        }

        double centimeters = (feet * 12) * 2.54;
        centimeters += inches * 2.54;
        System.out.println(feet + " feet, " + inches + " inches = " + centimeters + " cm");
        return centimeters;
    }

    public static double calculateFeetAndInchesToCentimeters(double inches){
        if(inches < 0){
            System.out.println("invalid input");
            return -1;
        }

        double feet = (int) inches / 12;
        double remainderInches = (int) inches % 12;

        System.out.println(inches + " inches is equal to " + feet + " feet and " + remainderInches + " inches");

        return calculateFeetAndInchesToCentimeters(feet, remainderInches);
    }
}
