package com.freakypunker;

public class Main {

    public static void main(String[] args) {
        Hamburger hamburger = new Hamburger("Basic","Sausage","White",3.5);

        double price = hamburger.itemizeHamburger();

        hamburger.addHamburgerAddition1("tomato",1.0);
        hamburger.addHamburgerAddition2("lettuce", 0.5);
        hamburger.addHamburgerAddition3("cheese",1.0);

        System.out.println("Total " + hamburger.getName() + " Burger is " + hamburger.itemizeHamburger());

        HealthyBurger healthyBurger = new HealthyBurger("Turkey",8.0);

        price = healthyBurger.itemizeHamburger();

        healthyBurger.addHealthyAddition1("eggs",1.5);
        healthyBurger.addHamburgerAddition1("tomato",1);
        healthyBurger.addHealthyAddition2("beans",0.5);

        System.out.println("Total " + healthyBurger.getName() + " Burger is " + healthyBurger.itemizeHamburger());

        DeluxeBurger deluxeBurger = new DeluxeBurger();

        deluxeBurger.addHamburgerAddition3("lettuce", 0.5);
        System.out.println("Total " + deluxeBurger.getName() + " Burger is " + deluxeBurger.itemizeHamburger());


    }
}
