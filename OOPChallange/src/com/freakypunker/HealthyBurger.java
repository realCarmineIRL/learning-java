package com.freakypunker;

public class HealthyBurger extends Hamburger {
    private String healthyExtraName1;
    private double healthyExtraPrice1;

    private String healthyExtraName2;
    private double healthyExtraPrice2;

    public HealthyBurger(String meat, double price) {
        super("Healthy", meat, "Brown Rye", price);
    }

    public void addHealthyAddition1(String name, double price) {
        this.healthyExtraName1 = name;
        this.healthyExtraPrice1 = price;
    }

    public void addHealthyAddition2(String name, double price) {
        this.healthyExtraName2 = name;
        this.healthyExtraPrice2 = price;
    }

    @Override
    public double itemizeHamburger() {

        double hamburgerPrice = super.itemizeHamburger();

        if(healthyExtraName1 != null){
            hamburgerPrice += this.healthyExtraPrice1;
            System.out.println("Added " + this.healthyExtraName1 + " for an extra price " + this.healthyExtraPrice1);
        }

        if(healthyExtraName2 != null){
            hamburgerPrice += this.healthyExtraPrice2;
            System.out.println("Added " + this.healthyExtraName2 + " for an extra price " + this.healthyExtraPrice2);
        }

        return hamburgerPrice;
    }
}
