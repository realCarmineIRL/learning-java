package com.freakypunker;

public class Hamburger {
    private String name;
    private String meat;
    private String breadRollType;
    private double price;

    private String additionName1;
    private double additionPrice1;

    private String additionName2;
    private double additionPrice2;

    private String additionName3;
    private double additionPrice3;

    private String additionName4;
    private double additionPrice4;

    public Hamburger(String name, String meat, String breadRollType, double price) {
        this.name = name;
        this.meat = meat;
        this.breadRollType = breadRollType;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void addHamburgerAddition1(String name, double price) {
        this.additionName1 = name;
        this.additionPrice1 = price;
    }

    public void addHamburgerAddition2(String name, double price) {
        this.additionName2 = name;
        this.additionPrice2 = price;
    }

    public void addHamburgerAddition3(String name, double price) {
        this.additionName3 = name;
        this.additionPrice3 = price;
    }

    public void addHamburgerAddition4(String name, double price) {
        this.additionName4 = name;
        this.additionPrice4 = price;
    }

    public double itemizeHamburger() {
        double hamburgerPrice = this.price;
        System.out.println(this.name + " " + this.meat + " Hamburger " + " on a " + this.breadRollType + " Roll, Price is " + this.price);

        if(additionName1 != null){
            hamburgerPrice += this.additionPrice1;
            System.out.println("Added " + this.additionName1 + " for an extra price " + this.additionPrice1);
        }

        if(additionName2 != null){
            hamburgerPrice += this.additionPrice2;
            System.out.println("Added " + this.additionName2 + " for an extra price " + this.additionPrice2);
        }

        if(additionName3 != null){
            hamburgerPrice += this.additionPrice3;
            System.out.println("Added " + this.additionName3 + " for an extra price " + this.additionPrice3);
        }

        if(additionName4 != null){
            hamburgerPrice += this.additionPrice4;
            System.out.println("Added " + this.additionName4 + " for an extra price " + this.additionPrice4);
        }

        return hamburgerPrice;
    }
}
