package com.freakypunker;

public class Main {

    public static void main(String[] args) {

        // Primitives
        // byte
        // short
        // int
        // long
        // float
        // double
        // char
        // boolean

        String myString = "this is a string";
        System.out.println("my string is equal to = " + myString);
        myString = myString + ", and this is more.";
        System.out.println("my string is equal to = " + myString);
        myString = myString + " \u00A9 2017";
        System.out.println("my string is equal to = " + myString);

        String lastString = "10";
        int myInt = 50;
        lastString = lastString + myInt;
        System.out.println("lastString is equal to = " + lastString);
    }
}
