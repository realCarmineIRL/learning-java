package com.freakypunker;

public class Main {

    public static void main(String[] args) {
        // int has a width of 32
	    int myMinValue = -2_147_483_648;
        int myMaxValue = -2_147_483_648;
        int myTotal = (myMinValue/2);
        System.out.println(myTotal);

        // byte has a width of 8
        byte myMinByteValue = -128;
        byte myMaxByteValue = 127;
        byte myNewByteValue = (byte)(myMinByteValue/2);
        System.out.println(myNewByteValue);

        // short has a width of 16
        short myMinShortValue = -32767;
        short myMaxShortValue = 32767;
        short myNewShortValue = (short)(myMinShortValue/2);
        System.out.println(myNewShortValue);

        // long has a width of 64
        long myMinLongValue = -9_223_372_036_854_775_807L;
        long myMaxLongValue = 9_223_372_036_854_775_807L;
        long myNewLongValue = (long)(myMinLongValue/2);
        System.out.println(myNewLongValue);

        byte byteValue = 10;
        short shortValue = 20;
        int intValue = 50;

        long longTotal = 50000L + 10L * (byteValue + shortValue + intValue);
        System.out.println("longTotal = " + longTotal);
    }
}
