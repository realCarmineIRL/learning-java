package com.freakypunker;

public class Main {

    public static void main(String[] args) {
	    int value = 3;

	    if(value == 1) {
            System.out.println("value was 1");
        } else if(value == 2) {
            System.out.println("value was 2");
        } else {
            System.out.println("value was not 1 or 2");
        }

        int switchValue = 3;

	    switch (switchValue){
            case 1:
                System.out.println("Value was 1");
                break;

            case 2:
                System.out.println("Value was 2");
                break;

            default:
                System.out.println("Value was not 1 or 3");
        }

        char switchCharValue = 'F';

	    switch (switchCharValue){
            case 'A':
                System.out.println("the value is " + switchCharValue);
                break;
            case 'B':
                System.out.println("the value is " + switchCharValue);
                break;
            case 'C':
                System.out.println("the value is " + switchCharValue);
                break;
            case 'D':
                System.out.println("the value is " + switchCharValue);
                break;
            case 'E':
                System.out.println("the value is " + switchCharValue);
                break;
            default:
                System.out.println("the value is not A, B, C, D or E");
                break;
        }

        String month = "January";

	    switch (month.toUpperCase()) {
            case "JANUARY":
                System.out.println("Jan");
                break;
            case "FEBRUARY":
                System.out.println("Feb");
                break;
            default:
                System.out.println("not jan or feb");
                break;
        }
    }
}
