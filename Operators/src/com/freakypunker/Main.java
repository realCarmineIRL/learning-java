package com.freakypunker;

public class Main {

    public static void main(String[] args) {
        int result = 1 + 2;
        System.out.println(result);

        int previousResult = result;

        result = result - 1;
        System.out.println(result);

        previousResult = result;
        result = result * 10;
        System.out.println(result);

        previousResult = result;
        result = result / 5;
        System.out.println(result);

        previousResult = result;
        result = result % 3;
        System.out.println(result);

        previousResult = result;
        result ++;
        result ++;
        System.out.println(result);

        previousResult = result;
        result --;
        System.out.println(result);

        previousResult = result;
        result += 2;
        System.out.println(result);

        previousResult = result;
        result *= 10;
        System.out.println(result);

        previousResult = result;
        result -= 10;
        System.out.println(result);

        previousResult = result;
        result /= 10;
        System.out.println(result);

        boolean isAlien = false;

        if (isAlien == true)
            System.out.println("it is not an alien");

        int topScore = 81;

        if (topScore < 100)
            System.out.println("you got the high score");

        int secondScore = 82;

        if (topScore > secondScore && topScore < 100)
            System.out.println("greater than second top score and less than a 100");

        int newValue = 50;
        if (newValue == 50)
            System.out.println("this is true");

        boolean isCar = true;
        if (isCar == true)
            System.out.println("this is not supposed to happen");

        boolean wasCar = isCar ? true : false;
        if(wasCar)
            System.out.println("wasCar is true");

        double myNewDouble = 20;
        double mySecondNewDouble = 80;

        double newResult = (myNewDouble + mySecondNewDouble) * 25;
        System.out.println(newResult);
        double theRemainder = newResult % 40;
        System.out.println(theRemainder);

        if(theRemainder <= 20)
            System.out.println("the total was over the limit");

        // http://cs.bilkent.edu.tr/~guvenir/courses/CS101/op_precedence.html
        // https://docs.oracle.com/javase/tutorial/java/nutsandbolts/opsummary.html
    }
}
